//
//  SearchViewController.swift
//  SoundCloudClientApp
//
//  Created by Aleksandr Aniskin on 11.10.2021.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var miniPlayerView: UIView!
    @IBOutlet weak var miniPlayerSongNameLabel: UILabel!
    @IBOutlet weak var miniPlayerSongAuthor: UILabel!
    @IBOutlet weak var miniPlayerWaveFormImageView: UIImageView!
    @IBOutlet weak var tssView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private var timer = Timer()
    
    var network = Networking()
    
    let plColor = UIColor.lightGray
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        network.controller = self
        
        setupSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        network.GetAccessTokenAF()
        tssView.isHidden = true
    }
    
    private func setupSearchBar() {
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search..."
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.tintColor = UIColor(named: "ActiveColor")
    }

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = network.tracks.isEmpty ? 1 : network.tracks.count
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "song", for: indexPath) as! TrackTableViewCell
        if network.tracks.isEmpty {
            cell.SongNameLabel.text = "Not find any tracks..."
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            cell.SongNameLabel.text = network.tracks[indexPath.row].title
            cell.SongAuthorLabel.text = network.tracks[indexPath.row].user.fullName
            if let imageUrl = network.tracks[indexPath.row].artworkURL {
                cell.SongIconImageView.loadImage(urlString: imageUrl)
            } else {
                cell.SongIconImageView.image = UIImage(named: "SongIcon")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        miniPlayerSongNameLabel.text = network.tracks[indexPath.row].title
        print(network.tracks[indexPath.row].title)
        miniPlayerSongAuthor.text = network.tracks[indexPath.row].user.fullName
        print(network.tracks[indexPath.row].user.fullName)
        miniPlayerWaveFormImageView.loadImage(urlString: network.tracks[indexPath.row].waveformURL ?? "")
        print(network.tracks[indexPath.row].waveformURL)
    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false, block: { (_) in
            print(searchText)
            self.network.GetTracksAF(queryString: searchText, token: self.network.accessToken)
        })
    }
}
