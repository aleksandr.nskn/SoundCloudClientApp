//
//  AccessTokenModel.swift
//  SoundCloudClientApp
//
//  Created by Aleksandr Aniskin on 03.11.2021.
//

import Foundation

struct AccessTokenModel: Codable {
    var accessToken: String //"2-255568--8YBgNES2VCeFbD87m7gK1fm"
    var expiresIn: Int //3599
    var refreshToken: String //"FukFM2ArQHVgpocK6R5RHjMTVZ4aapgq"
    var scope: String //""
    var tokenType: String //"bearer"
    
    //CodingKeys
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
        case scope
        case tokenType = "token_type"
    }
    
    init() {
        self.accessToken = ""
        self.expiresIn = 0
        self.refreshToken = ""
        self.scope = ""
        self.tokenType = ""
    }
}

struct Oauth2Model {
    var grantType: String //"client_credentials"
    var clientId: String //"08cb4bc9efc7ebeb5945abe37ae11b39"
    var clientSecret: String //"43d7b47398b1e57bf05a6f8ce0cc8a49"
    
    //CodingKeys
    enum CodingKeys: String, CodingKey {
        case grantType = "grant_type"
        case clientId = "client_id"
        case clientSecret = "client_secret"
    }
    
    init() {
        self.grantType = "client_credentials"
        self.clientId = "08cb4bc9efc7ebeb5945abe37ae11b39"
        self.clientSecret = "43d7b47398b1e57bf05a6f8ce0cc8a49"
    }
}

