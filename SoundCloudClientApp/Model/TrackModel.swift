//
//  TrackModel.swift
//  SoundCloudClientApp
//
//  Created by Aleksandr Aniskin on 07.11.2021.

import Foundation

// MARK: - TrackModel
class TrackModel: Codable {
    let collection: [Collection]?
    let nextHref: String?

    init(collection: [Collection]?, nextHref: String?) {
        self.collection = collection
        self.nextHref = nextHref
    }
}

// MARK: - Collection
class Collection: Codable {
    let kind: String?
    let id: Int?
    let createdAt: String?
    let duration: Int?
    let commentable: Bool?
    let commentCount: Int?
    let sharing, tagList: String?
    let streamable: Bool?
    let embeddableBy: String?
    let purchaseURL: String?
    let purchaseTitle: String?
    let genre, title, collectionDescription: String?
    let labelName, release, keySignature, isrc: String?
//    let bpm, releaseYear, releaseMonth, releaseDay: String?
    let license: String?
    let uri: String?
    let user: User
    let permalinkURL: String?
    let artworkURL: String?
    let streamURL: String?
    let downloadURL: String?
    let waveformURL: String?
    let availableCountryCodes, secretURI, userFavorite, userPlaybackCount: String?
    let playbackCount, downloadCount, favoritingsCount, repostsCount: Int?
    let downloadable: Bool?
    let access: String?
    let policy, monetizationModel: String?
    
    enum CodingKeys: String, CodingKey {
        case kind, id
        case createdAt = "created_at"
        case duration, commentable
        case commentCount = "comment_count"
        case sharing
        case tagList = "tag_list"
        case streamable
        case embeddableBy = "embeddable_by"
        case purchaseURL = "purchase_url"
        case purchaseTitle = "purchase_title"
        case genre, title
        case collectionDescription = "description"
        case labelName = "label_name"
        case release
        case keySignature = "key_signature"
        case isrc //bpm
//        case releaseYear = "release_year"
//        case releaseMonth = "release_month"
//        case releaseDay = "release_day"
        case license, uri, user
        case permalinkURL = "permalink_url"
        case artworkURL = "artwork_url"
        case streamURL = "stream_url"
        case downloadURL = "download_url"
        case waveformURL = "waveform_url"
        case availableCountryCodes = "available_country_codes"
        case secretURI = "secret_uri"
        case userFavorite = "user_favorite"
        case userPlaybackCount = "user_playback_count"
        case playbackCount = "playback_count"
        case downloadCount = "download_count"
        case favoritingsCount = "favoritings_count"
        case repostsCount = "reposts_count"
        case downloadable, access, policy
        case monetizationModel = "monetization_model"
    }

    init(kind: String?, id: Int?, createdAt: String?, duration: Int?, commentable: Bool?, commentCount: Int?, sharing: String?, tagList: String?, streamable: Bool?, embeddableBy: String?, purchaseURL: String?, purchaseTitle: String?, genre: String?, title: String?, collectionDescription: String?, labelName: String?, release: String?, keySignature: String?, isrc: String?, license: String?, uri: String?, user: User, permalinkURL: String?, artworkURL: String?, streamURL: String?, downloadURL: String?, waveformURL: String?, availableCountryCodes: String?, secretURI: String?, userFavorite: String?, userPlaybackCount: String?, playbackCount: Int?, downloadCount: Int?, favoritingsCount: Int?, repostsCount: Int?, downloadable: Bool?, access: String?, policy: String?, monetizationModel: String?) {
        self.kind = kind
        self.id = id
        self.createdAt = createdAt
        self.duration = duration
        self.commentable = commentable
        self.commentCount = commentCount
        self.sharing = sharing
        self.tagList = tagList
        self.streamable = streamable
        self.embeddableBy = embeddableBy
        self.purchaseURL = purchaseURL
        self.purchaseTitle = purchaseTitle
        self.genre = genre
        self.title = title
        self.collectionDescription = collectionDescription
        self.labelName = labelName
        self.release = release
        self.keySignature = keySignature
        self.isrc = isrc
//        self.bpm = bpm
//        self.releaseYear = releaseYear
//        self.releaseMonth = releaseMonth
//        self.releaseDay = releaseDay
        self.license = license
        self.uri = uri
        self.user = user
        self.permalinkURL = permalinkURL
        self.artworkURL = artworkURL
        self.streamURL = streamURL
        self.downloadURL = downloadURL
        self.waveformURL = waveformURL
        self.availableCountryCodes = availableCountryCodes
        self.secretURI = secretURI
        self.userFavorite = userFavorite
        self.userPlaybackCount = userPlaybackCount
        self.playbackCount = playbackCount
        self.downloadCount = downloadCount
        self.favoritingsCount = favoritingsCount
        self.repostsCount = repostsCount
        self.downloadable = downloadable
        self.access = access
        self.policy = policy
        self.monetizationModel = monetizationModel
    }
}

// MARK: - User
class User: Codable {
    let avatarURL: String?
    let id: Int?
    let kind: String?
    let permalinkURL, uri: String?
    let username, permalink, createdAt, lastModified: String?
    let firstName, lastName, fullName: String?
    let city, userDescription, country: String?
    let trackCount, publicFavoritesCount, repostsCount, followersCount: Int?
    let followingsCount: Int?
    let plan: String?
    let myspaceName: String?
    let discogsName: String?
    let websiteTitle: String?
    let website: String?
    let commentsCount: Int?
    let online: Bool?
    let likesCount, playlistCount: Int?
    let subscriptions: [Subscription]?
    
    enum CodingKeys: String, CodingKey {
        case avatarURL = "avatar_url"
        case id, kind
        case permalinkURL = "permalink_url"
        case uri, username, permalink
        case createdAt = "created_at"
        case lastModified = "last_modified"
        case firstName = "first_name"
        case lastName = "last_name"
        case fullName = "full_name"
        case city
        case userDescription = "description"
        case country
        case trackCount = "track_count"
        case publicFavoritesCount = "public_favorites_count"
        case repostsCount = "reposts_count"
        case followersCount = "followers_count"
        case followingsCount = "followings_count"
        case plan
        case myspaceName = "myspace_name"
        case discogsName = "discogs_name"
        case websiteTitle = "website_title"
        case website
        case commentsCount = "comments_count"
        case online
        case likesCount = "likes_count"
        case playlistCount = "playlist_count"
        case subscriptions
    }

    init(avatarURL: String?, id: Int?, kind: String?, permalinkURL: String?, uri: String?, username: String?, permalink: String?, createdAt: String?, lastModified: String?, firstName: String?, lastName: String?, fullName: String?, city: String?, userDescription: String?, country: String?, trackCount: Int?, publicFavoritesCount: Int?, repostsCount: Int?, followersCount: Int?, followingsCount: Int?, plan: String?, myspaceName: String?, discogsName: String?, websiteTitle: String?, website: String?, commentsCount: Int?, online: Bool?, likesCount: Int?, playlistCount: Int?, subscriptions: [Subscription]?) {
        self.avatarURL = avatarURL
        self.id = id
        self.kind = kind
        self.permalinkURL = permalinkURL
        self.uri = uri
        self.username = username
        self.permalink = permalink
        self.createdAt = createdAt
        self.lastModified = lastModified
        self.firstName = firstName
        self.lastName = lastName
        self.fullName = fullName
        self.city = city
        self.userDescription = userDescription
        self.country = country
        self.trackCount = trackCount
        self.publicFavoritesCount = publicFavoritesCount
        self.repostsCount = repostsCount
        self.followersCount = followersCount
        self.followingsCount = followingsCount
        self.plan = plan
        self.myspaceName = myspaceName
        self.discogsName = discogsName
        self.websiteTitle = websiteTitle
        self.website = website
        self.commentsCount = commentsCount
        self.online = online
        self.likesCount = likesCount
        self.playlistCount = playlistCount
        self.subscriptions = subscriptions
    }
}

// MARK: - Subscription
class Subscription: Codable {
    let product: Product?

    init(product: Product?) {
        self.product = product
    }
}

// MARK: - Product
class Product: Codable {
    let id, name: String?

    init(id: String?, name: String?) {
        self.id = id
        self.name = name
    }
}


