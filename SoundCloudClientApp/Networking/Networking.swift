//
//  Networking.swift
//  TestAlamofire
//
//  Created by Aleksandr Aniskin on 12.11.2021.
//

import Foundation
import Alamofire

class Networking {
    
    weak var controller: SearchViewController?
    var accessToken: String = ""
    var oauthModel = Oauth2Model()
    var tracks: [Collection] = []
    
    func GetAccessTokenAF(){
        let urlApi = "https://api.soundcloud.com/oauth2/token?"
        let parameters = ["grant_type":oauthModel.grantType,
                          "client_id":oauthModel.clientId,
                          "client_secret":oauthModel.clientSecret]
        AF.request(urlApi,
                   method: .post,
                   parameters: parameters).responseDecodable(of: AccessTokenModel.self) { (response) in
            switch response.result{
                case.success(let response):
                    DispatchQueue.main.async{
                        let token = response.accessToken
                        self.accessToken = token
                    }
                case .failure(let error):
                    print("Error: ",error)
            }
            
        }
    }
    
    func GetTracksAF(queryString: String, token: String){
        let urlApi = "https://api.soundcloud.com/tracks?"
        let parameters = ["q":"\(queryString)",
                          "limit":"50",
                          "access":"",
                          "linked_partitioning":"true"]
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        print(headers)
        
        AF.request(urlApi,
                   method: .get,
                   parameters: parameters,
                   headers: headers).responseDecodable(of: TrackModel.self) { (response) in
            switch response.result{
                case.success(let tracksResponse):
                    DispatchQueue.main.async{
                        if let tracks = tracksResponse.collection {
                            self.tracks = tracks
                            print(self.tracks.map { $0.waveformURL })
                        }
                        self.controller?.tableView.reloadData()
                        self.controller?.tssView.isHidden = false
                    }
                case .failure(let error):
                    print("Error: ",error)
                    self.tracks = []
                    self.controller?.tableView.reloadData()
                    self.controller?.tssView.isHidden = true
            }
            debugPrint(response)
            
        }
    }
    
}
