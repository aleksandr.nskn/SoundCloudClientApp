//
//  ExtensionUIImageView.swift
//  BSWStore
//
//  Created by Aleksandr Aniskin on 02.08.2021.
//

import Foundation
import UIKit


class CornerRadiusForImageView: UIImageView {
    @IBInspectable
    var radiusForCorner: CGFloat = 4 {
        didSet {
            layer.cornerRadius = radiusForCorner
        }
    }
}

extension UIImageView {
    
    func loadImage(urlString: String)  {
        guard let url =  URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, respones, error) in
            if error != nil {
                print(error ?? "")
                return
            }
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }).resume()
    }
}
