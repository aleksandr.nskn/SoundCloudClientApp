//
//  TrackTableViewCell.swift
//  SoundCloudClientApp
//
//  Created by Aleksandr Aniskin on 07.11.2021.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var SongAuthorLabel: UILabel!
    @IBOutlet weak var SongNameLabel: UILabel!
    @IBOutlet weak var SongIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
